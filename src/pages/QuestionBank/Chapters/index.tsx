// ** MUI Imports
import Grid from '@mui/material/Grid'
import MuiLink from '@mui/material/Link'
import Typography from '@mui/material/Typography'

// ** Custom Components Imports
import PageHeader from 'src/@core/components/page-header'

// ** Demo Components Imports
import TableServerSide from 'src/views/table/data-grid/TableServerSide'

const Chapters = () => {
  return (
    <Grid container spacing={6}>
      <PageHeader
        title={
          <Typography variant='h5'>
            <MuiLink href='#' target='_blank'>
              Chapters Management
            </MuiLink>
          </Typography>
        }
        subtitle={
          <Typography variant='body2'>
            All chapters can be managed through this module
          </Typography>
        }
      />
      <Grid item xs={12}>
        <TableServerSide />
      </Grid>
    </Grid>
  )
}

export default Chapters
